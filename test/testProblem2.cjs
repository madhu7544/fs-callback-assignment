
const problem2Result = require("../problem2.cjs");

const lipsumFile = "../data/lipsum.txt";

problem2Result (lipsumFile,(err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
