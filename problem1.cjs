const fs = require('fs');
const path = require('path');
const directoryPath = './tempFiles';


function createFile(n, callback){
    fs.mkdir(directoryPath, (err) => {
        if (err) {
            callback(err);
        } else {
            console.log('Directory created');
            let count = 0;
            for (let i = 0; i < n; i++) {
                const filepath = path.join(directoryPath, `file${i}.json`);
                const data = JSON.stringify({ id: i, name: `file${i}`});
                fs.writeFile(filepath, data, (err) => {
                    if (err) {
                        console.error(`Error creating file ${filename}: ${err}`);
                    } else {
                        console.log(`File created: ${filepath}`);
                    }
                });
                count++;
                if (count === n) {
                    deleteDirFile(callback);
                }
                
            }
        }
    });
};

function deleteDirFile(callback){
    fs.readdir(directoryPath, (err, files) => {
        if (err) {
            console.error(`Error reading directory: ${err}`);
            callback(err);
        } else {
            let count = 0;
            files.forEach((file) => {
                const filepath = path.join(directoryPath, file);
                fs.unlink(filepath, (err) => {
                    if (err) {
                        console.error(`Error deleting file ${file}: ${err}`);
                        callback(err);
                    } else {
                        console.log(`File deleted: ${filepath}`);
                        count++;
                        if (count === files.length) {
                            callback(null);
                        }
                    }
                })
            });
        }
    });
};
    

module.exports = createFile