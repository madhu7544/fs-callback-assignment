const fs = require('fs');
const path = require('path');

function problem2 (lipsumFile,callback){
  readingFile(lipsumFile, (err, data) => {
    if (err) {
      callback(err);
    } else {
      let upperData = data.toUpperCase();
      //storing data in new file
      writingFile(upperData, (err) => {
        if (err) {
          callback(err);
        } else {
          //getting the new file
          readNewFileData((err,data) => {
            if (err) {
              callback(err);
            } else {
              let lowerData = data.toLowerCase()
              let tempdata = lowerData.split('.').join('\n')
              //converted to sentences and stored that file
              writingFile(tempdata, (err) => {
                if (err) {
                  callback(err);
                } else {
                  readNewFileData((err, data) => {
                    if (err) {
                      callback(err);
                    } else {
                      let tempdata = data.split("\n").sort().join("\n");
                      //sorted the data and stored
                      writingFile(tempdata, (err) => {
                        if (err) {
                          callback(err);
                        } else {
                          readNewFileData((err, data) => {
                            if (err) {
                              callback(err);
                            } else {
                              callback(null, data);
                              //delete the file
                              deleteFile((err, data) => {
                                if (err) {
                                  callback(err);
                                } else {
                                  callback(null, data);
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
      });
    };

function readingFile(lipsumFile, callback) {
  fs.readFile(lipsumFile, "utf-8", (err, data) => {
    if (err) {
      callback(new Error("There is no File!"));
    } else {
      callback(null, data);
    }
  });
}

function writingFile(upperData, callback) {
  fs.writeFile(path.join(__dirname, "./data/filenames.txt"),upperData,(err) => {
      if (err) {
        callback(err);
      } else {
        callback(null, "File created successfully!");
      }
    }
  );
}

function readNewFileData(callback) {
  fs.readFile(path.join(__dirname, "./data/filenames.txt"),"utf-8",(err, data) => {
      if (err) {
        callback(err);
      } else {
        callback(null, data);
      }
    }
  );
}

function deleteFile(callback) {
  fs.unlink(path.join(__dirname, "./data/filenames.txt"), (err) => {
    if (err) {
      callback(new Error("File not exists!"));
    } else {
      callback(null, "File deleted successfully!");
    }
  });
}

module.exports = problem2
