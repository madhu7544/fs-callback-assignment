const problem1Result = require('../problem1.cjs');

problem1Result(3, (err) => {
    if (err) {
        console.error(`Error: ${err}`);
    } else {
        console.log('All files deleted successfully');
    }
});
